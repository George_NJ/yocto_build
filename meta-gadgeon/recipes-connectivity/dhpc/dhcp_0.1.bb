SUMMARY = "DHCP server"
DESCRIPTION = "run a DHCP server for giving ip to connected mobile"
LICENSE = "CLOSED"

TARGET_CC_ARCH += "${LDFLAGS}"
RDEPENDS_${PN} += "bash"

SRC_URI = "file://gadgeon_dhcp_startup.sh \
           file://gadgeon_dhcp.conf \
           file://gadgeon_dhcp.service "

inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "gadgeon_dhcp.service"

FILES_${PN} += "/home/root"

do_install () {
    install -d ${D}${sysconfdir}
    install -m 0755 ${WORKDIR}/gadgeon_dhcp.conf ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/systemd/system
    install -m 0644 ${WORKDIR}/gadgeon_dhcp.service \
      ${D}${sysconfdir}/systemd/system
    install -d ${D}/home/root/
    install -m 0755 ${WORKDIR}/gadgeon_startup.sh ${D}/home/root/
}
