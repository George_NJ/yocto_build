# This README file contains information on the yocto-build.
# Please see the corresponding sections below for details.

## Files
        meta-gadgeon (contains the reference configuration files)
        config.sh (script used to clone and bitbake the custom yocto image using the created "Config.cfg")
        menu.py (python code used to create the "Config.cfg")
        README.md (this file containing all usages)
        sample_Config.cfg (a sample of "Config.cfg")

## Usage
        Step 1 : python3 menu.py (a menu is pop-up where you can add your requirements for the project)
        Step 2 : ./config.sh (build automatically starts with your custom configuration)

