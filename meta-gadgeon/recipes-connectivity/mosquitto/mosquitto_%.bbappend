FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://mosquitto.conf \
            file://conf.d "

do_install_append () {
    install -d ${D}${sysconfdir}/mosquitto
    install -m 0644 ${WORKDIR}/mosquitto.conf \
                ${D}${sysconfdir}/mosquitto/mosquitto.conf
    cp -r ${WORKDIR}/conf.d ${D}${sysconfdir}/mosquitto
}
