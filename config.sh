#!/bin/bash

#Read the configuration file
source Config.cfg

inital_configurations()
{
    ref_layer="$(pwd)/meta-gadgeon"
#    mkdir "yocto_dir"
    cd yocto_dir
    root=$(pwd)
}

clone_repo()
{
    git_cmd="/usr/bin/git"
    [ ! -f "${git_cmd}" ] && echo "Please install git tools (apt install git)" 1 && exit 1

    for((i=1;i<=$CONFIG_REPO_COUNT;i++))
    do
        gitrepoLink=$(echo 'CONFIG_REPO_LINK_'$i)
        gitrepoBrank=$(echo 'CONFIG_REPO_BRANCH_'$i)
        gitrepoCommit=$(echo 'CONFIG_REPO_COMMIT_PONT_'$i)
        if [[ ! -z "${!gitrepoLink}" ]];
        then
            cd $root && git clone ${!gitrepoLink}
            if [[ $? -ne 0 ]];
            then
               echo "Please check the repository url"
               exit 1
            fi
            gitrepo=$(echo ${!gitrepoLink} | rev | cut -d"/" -f1  | cut -d"." -f2 | rev)
            cd $gitrepo && git checkout -b ${!gitrepoBrank}-new ${!gitrepoCommit}
        fi
    done
}

build_env()
{
    cd "$root" && source poky/oe-init-build-env >/dev/null 2>&1

    bitbake-layers create-layer ../$CONFIG_META_LAYER_NAME
    meta_layer=$root/$CONFIG_META_LAYER_NAME

    #Update the layers to the bblayers.conf
    cd "$root/build/conf" && sed -i '$ d' bblayers.conf
    for((i=1;i<=$CONFIG_LAYERS_COUNT;i++))
    do
        layersPath=$(echo 'CONFIG_PATH_TO_LAYER_'$i)
        if [[ ! -z "${!layersPath}" ]];
        then
            echo '  '$root'/'${!layersPath}' \' >> bblayers.conf
        fi
    done
    echo '  "' >> bblayers.conf

    #Update the machine name in local.conf
    cd "$root/build/conf" && sed -i '20iMACHINE ?= "'$CONFIG_MACHINE'"' local.conf

    #update service manager in local.conf
    cd "$root/build/conf"
    if [[ "${CONFIG_SERVICE_MANAGER}" == "systemd" ]];
    then
            echo 'DISTRO_FEATURES_append = " systemd"' >> local.conf
            echo 'DISTRO_FEATURES_BACKFILL_CONSIDERED += "sysvinit"' >> local.conf
            echo 'VIRTUAL-RUNTIME_init_manager = "systemd"' >> local.conf
            echo 'VIRTUAL-RUNTIME_initscripts = "systemd-compat-units"' >> local.conf
    elif [[ "${CONFIG_SERVICE_MANAGER}" == "sysvinit" ]];
    then
        echo 'DISTRO_FEATURES_append = " sysvinit"' >> local.conf
        echo 'DISTRO_FEATURES_BACKFILL_CONSIDERED_append = " systemd"' >> local.conf
        echo 'VIRTUAL-RUNTIME_init_manager = "sysvinit"' >> local.conf
        echo 'VIRTUAL-RUNTIME_initscripts = "initscripts"' >> local.conf

    fi

    #update image file types
    filetypes=$(echo "${CONFIG_IMAGE_FILE_TYPE}" | tr , \ )
    echo 'IMAGE_FSTYPES += "'$filetypes'"' >> local.conf
}

create_recipes_application()
{
    cd $meta_layer/recipes-application
    mkdir $CONFIG_APPLICATION_NAME
    cd $CONFIG_APPLICATION_NAME
    mkdir files
    cp $ref_layer/recipes-application/gadgeon-app/gadgeon-app_0.1.bb ${CONFIG_APPLICATION_NAME}_0.1.bb
    sed -i "/^FILES_\${PN} +=.*/i APP_LOC = \"${CONFIG_TARGET_LOCATION}\" " ${CONFIG_APPLICATION_NAME}_0.1.bb
}

create_recipes-connectivity()
{
    cd $meta_layer/recipes-connectivity
    generalPackage=(${CONFIG_GENERAL_PACKAGES//,/ })
    packageCount=${#generalPackage[@]}
    for((index=0;index<$packageCount;index++))
    do
    case ${generalPackage[index]} in

            'Hostapd')
              cp -r $ref_layer/recipes-connectivity/hostapd .
              ;;

            'Mosquitto')
              cp -r $ref_layer/recipes-connectivity/mosquitto .
              ;;

            'DHCP')
              cp -r $ref_layer/recipes-connectivity/dhpc .
              ;;

      esac
   done
}

rename_hostname()
{
    cd $meta_layer/recipes-core
    mkdir "base-files"
    echo "hostname=\"$CONFIG_HOST_NAME\"" >> base-files/base-files_%.bbappend
}

create_image_file()
{
    cd $meta_layer/recipes-core
    mkdir "images"
    cp $ref_layer/recipes-core/images/gadgeon-image.bb images/$CONFIG_HOST_NAME-image.bb
}

add_packages()
{   
    generalPackage=(${CONFIG_GENERAL_PACKAGES//,/ })
    packageCount=${#generalPackage[@]}
    cd $meta_layer/recipes-core/images
    for((index=0;index<$packageCount;index++))
    do
   	case ${generalPackage[index]} in

   			'Hostapd')
              sed -i "/^IMAGE_INSTALL =.*/a \\\t\t hostapd \\\ " $CONFIG_HOST_NAME-image.bb
   			  ;;
   			  
   			'Mosquitto')
              sed -i "/^IMAGE_INSTALL =.*/a \\\t\t mosquitto libmosquitto1 libmosquittopp1 mosquitto-dev \\\ " $CONFIG_HOST_NAME-image.bb
   			  ;;
   			  
   			'DHCP')
              sed -i "/^IMAGE_INSTALL =.*/a \\\t\t dhcpcd \\\ " $CONFIG_HOST_NAME-image.bb
   			  ;;
   			  
   			*)
              sed -i "/^IMAGE_INSTALL =.*/a \\\t\t ${generalPackage[index]} \\\ " $CONFIG_HOST_NAME-image.bb
   			  ;;

	  esac
   done
}

create_recipes_core()
{
    rename_hostname
    create_image_file
    add_packages
}

wifi_kernel_module()
{
    cp $ref_layer/recipes-kernel/gadgeon_%.bbappend $meta_layer/recipes-kernel/linux/linux-%.bbappend
    kernelPackage=(${CONFIG_WIFI_MODULES//,/ })
    packageCount=${#kernelPackage[@]}  
    for((index=0;index<$packageCount;index++))
    do
        case ${kernelPackage[index]} in
            'nl80211')
            sed -i "/^SRC_URI +=.*/a \\\t file://nl80211.cfg \\\ " $meta_layer/recipes-kernel/linux/linux-%.bbappend
            cp $ref_layer/recipes-kernel/files/nl80211.cfg $meta_layer/recipes-kernel/linux/files
        esac     
    done
}

add_kernel_modules()
{
    wifi_kernel_module
}

create_recipes_kernel()
{
    cd $meta_layer/recipes-kernel
    mkdir "linux"
    cd linux
    mkdir "files"
    cp $ref_layer/recipes-kernel/gadgeon_%.bbappend $meta_layer/recipes-kernel/linux/linux-%.bbappend
    add_kernel_modules
}

custom_meta_layer()
{
    cd $meta_layer
    mkdir "recipes-application"
    mkdir "recipes-connectivity"
    mkdir "recipes-core"
    mkdir "recipes-kernel"
    create_recipes_application
    create_recipes-connectivity
    create_recipes_core
    create_recipes_kernel
}

#Script Start
inital_configurations

#Clone the repos
#clone_repo

#Initialize the build environment
build_env

#Adding custom layer
custom_meta_layer

#Start the build
echo "*********************************"
echo "Now you can do \"bitbake $CONFIG_HOST_NAME-image\""
echo "*********************************"
echo "Sample configuration files are given, which you can overwrite with yours"
