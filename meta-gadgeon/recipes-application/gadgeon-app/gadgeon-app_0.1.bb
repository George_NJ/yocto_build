SUMMARY = "Firmware application"
DESCRIPTION = "install application into the device"
LICENSE = "CLOSED"

TARGET_CC_ARCH += "${LDFLAGS}"
RDEPENDS_${PN} += "bash"

SRC_URI = "file://app.tar \
           "

S = "${WORKDIR}/app"

FILES_${PN} += "${APP_LOC}"

do_install () {
# if you are using a Makefile, use the help of the command below;
#    oe_runmake install DESTDIR=${D} BINDIR=${bindir} SBINDIR=${sbindir} \
#      MANDIR=${mandir} INCLUDEDIR=${includedir}
# if only have a single file (like a c or cpp) use the below;
#    ${CC} file.c -o obj
#    ${CXX} file.cpp -o obj  
    install -d ${D}${APP_LOC}
    install -m 0755 ${WORKDIR}/<object-file> ${D}${APP_LOC}
}

