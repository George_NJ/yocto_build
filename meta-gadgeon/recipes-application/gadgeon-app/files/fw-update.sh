#!/bin/bash

#This script is used for firmware update of gadgeon app

APP_PATH=/home/root
obj_name="gadgeon-app"
obj_tar_name="gadgeonApp.tar.gz"


download_app()
{
    rm /tmp/$obj_tar_name 2>/dev/null
    download_obj=$1
    curl $parameters $download_obj $certs -o /tmp/$obj_tar_name 2>/dev/null
    if [ ! -f /tmp/$obj_tar_name ]; then
        echo "1" > $APP_PATH/updated.txt
        echo "Downloading failed" >> $APP_PATH/fw-update.log
        reboot
        exit 1
    else
        echo "downloaded new application" >> $APP_PATH/fw-update.log
    fi
}

check_cheksum()
{
    file_checksum=`md5sum /tmp/$obj_tar_name | awk '{print $1;}'`
    if [ "$checksum" != "$file_checksum" ]; then
        echo "Different checksum" >> $APP_PATH/fw-update.log
        reboot
        exit 1
    else
        echo "checksum is ok" >> $APP_PATH/fw-update.log
    fi
}

update_app()
{
    mv $APP_PATH/$obj_name $APP_PATH/bkup_obj 2>/dev/null
    tar -xvf /tmp/$obj_tar_name 2>/dev/null
    chmod +x $obj_name 2>/dev/null
    mv $obj_name $APP_PATH/$obj_name 2>/dev/null

    $APP_PATH/$obj_name &
    sleep 10

    pid=$(pidof $obj_name)
    if [ $? -eq 0 ]; then
        echo "0" > $APP_PATH/updated.txt
        echo "updated application" >> $APP_PATH/fw-update.log
    else
        mv $APP_PATH/bkup_obj $APP_PATH/$obj_name 2>/dev/null
        echo "1" > $APP_PATH/updated.txt
        echo "Update not completed" >> $APP_PATH/fw-update.log
    fi
}


echo " Firmware update log..." > $APP_PATH/fw-update.log

touch $APP_PATH/updated.txt 2>/dev/null
echo "1" > $APP_PATH/updated.txt

temp=`getopt -o D:C: --long download_path:,checksum: --name "gadgeon error" -- "$@"`
[ $? != 0 ] && exit 1
eval set -- "$temp"

[ $# -ne 5 ] && echo "Not enough arguments" >> $APP_PATH/fw-update.log && exit 1

while true; do
    case "$1" in
        -D|--download_path)    download_path="$2"; shift 2;;
        -C|--checksum)         checksum="$2"; shift 2;;
        --)                    shift; break;;
        *)                     echo "Internal error" >> $APP_PATH/fw-update.log;;
    esac
done

echo "download path: " $download_path >> $APP_PATH/fw-update.log
echo "checksum: " $checksum >> $APP_PATH/fw-update.log

download_app $download_path
check_cheksum

pid=$(pidof $obj_name)
if [ $? -eq 0 ]; then
    { kill -9 $pid && wait $pid; } 2>/dev/null
fi

update_app

echo "Rebooting..." >> $APP_PATH/fw-update.log;
reboot
