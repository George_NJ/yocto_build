#! /usr/bin/env python3

from dialog import Dialog
import os

d = Dialog(dialog="dialog")
config_list = {}
general_packages = {}

class menu():
    level1 = 1
    level2 = 2
    level3 = 3
    level4 = 4

def exit_menu():
    if d.yesno("Do you want to exit?") == d.OK:
        os.system('clear')
        exit(0)
    else:
        pass

def save():
    if d.yesno("Are you sure to save the progress and start build?") == d.OK:
        filepath = os.getcwd() + "/Config.cfg"
        with open(filepath, 'w+') as file:
            for key in config_list:
                if not isinstance(config_list[key], list):
                    file.write("%s=%s\n" % (key, str(config_list[key]).strip()))
                else:
                    file.write("%s=%s\n" % (key, ','.join(config_list[key])))
        #file.close
        os.system('clear')
        print("File created: " + filepath)
        #exit(0)
    else:
        os.system('clear')
        print("File not created")
        exit(0)

def start_build():
    print("starting build")
    cmd = 'chmod 777 config.sh;./config.sh'
    os.system(cmd)
    exit(0)

def main_menu():
    choices = [("Meta-Layer Name -->",''),("Extra Repositories to be Pulled -->",''),("Layers to be Added -->",''),
               ("Firmware Application Integration -->",''),("Required Packages -->",''),
               ("Kernel Configuration -->",''),("General Machine Settings --->",''),("    Quit...",'')]
    code, tag = d.menu(
        "Arrow keys navigate the menu.<Enter> selects submenus ---> "
        "(or empty submenus ----). Highlighted letters are hotkeys. Press <Esc><Esc> or cancel to exit",
        choices=choices, title="Yocto Configuration", ascii_lines=True,
        height=len(choices) + 12, menu_height=len(choices) + 4, extra_button=True,
        extra_label="Save/Build", width=70)

    if code == d.OK:
        if choices.index((tag, '')) == len(choices)-1:
            #exit_menu()
            return -1
        else:
            #option = (ord(tag)-ord('a')+1)
            option=choices.index((tag,''))+1
            return option
    elif code == "extra":
        save()
        start_build()
    else:
        return -1

def update_selected_packages(selected_packages,available_packages):
    pending_packages = list(set(available_packages)-set(selected_packages))
    for _ in selected_packages:
        general_packages[_] = True
    for i in pending_packages:
        general_packages[i] = False

def genearl_packages():
    packages_list = ['Hostapd', 'DHCP', 'Mosquitto','WPA supplicant']
    code, tags = d.checklist("Arrow keys navigate the menu. use space to select or deselect",
                             choices=[("Hostapd", "", general_packages.get("Hostapd", False)),
                                      ("DHCP", "", general_packages.get("DHCP", False)),
                                      ("Mosquitto", "", general_packages.get("Mosquitto", False)),
                                      ("WPA supplicant", "", general_packages.get("WPA supplicant", False))],
                             title='General Packages')
    if code == d.OK:
        config_list['CONFIG_GENERAL_PACKAGES'] = tags
        update_selected_packages(tags, packages_list)

    else:
        pass

def package_manager():
    packages_list=['apt','dpkg','aptitude']
    code, tags = d.checklist("Arrow keys navigate the menu. use space to select or deselect",
                             choices=[("apt", "", general_packages.get("apt", False)),
                                      ("dpkg", "", general_packages.get("dpkg", False)),
                                      ("aptitude", "", general_packages.get("aptitude", False))],
                             title="Package Manager")
    if code == d.OK:
        config_list['CONFIG_PCKGS']=tags
        update_selected_packages(tags,packages_list)
    else:
        pass

def required_pakages():
    choices = [("Package Manager -->", ''), ("Selection for required Packages -->", '')]
    code, tag = d.menu(
        "Arrow keys navigate the menu.<Enter> selects submenus ---> "
        "(or empty submenus ----). Highlighted letters are hotkeys. Cancel to go back",
        choices=choices, title="Required Packages", ascii_lines=True,
        height=len(choices) + 12, menu_height=len(choices) + 4, width=70)
    if code == d.OK:
        option = choices.index((tag, '')) + 1
        if option == 1:
            package_manager()
        elif option == 2:
            genearl_packages()
        required_pakages()
    else:
        pass

def extra_repo(position):
    elements = [
        ("Link to the repo", 1, 1,config_list.get('CONFIG_REPO_LINK_'+str(position+1),''), 1, 25, 60, 150, 0x0),
        ("Branch", 2, 1, config_list.get('CONFIG_REPO_BRANCH_' + str(position+1),''), 2, 25, 60, 60, 0x0),
        ("Commit point", 3, 1, config_list.get('CONFIG_REPO_COMMIT_PONT_' + str(position+1),''), 3, 25, 60, 150, 0x0)
        ]
    code, fields = d.mixedform(
        'Use arrow keys to navigate the menu. Press <tab> to navigate the buttons. '
        'Press "OK" to record entry and "cancel" to go back', elements, width=70,height=len(elements)+10,
        title='Configure Extra Repo '+str(position+1),form_height=len(elements)+3)
    if code == d.OK:
        if all(fields):
            config_list['CONFIG_REPO_LINK_'+str(position+1)] = fields[0]
            config_list['CONFIG_REPO_BRANCH_' + str(position+1)] = fields[1]
            config_list['CONFIG_REPO_COMMIT_PONT_' + str(position+1)] = fields[2]
            return True
        else:
            d.msgbox("All fields are mandatory!!")
            extra_repo(position)
    else:
        return False

def extra_layer(position):
    code, path_to_metalayer = d.inputbox("Enter path relative to pwd -> yocto_dir"
                                         " Eg:'meta-gadgeon' or 'poky/meta-gadgeon'"
                                         "\nPress OK to record entry and cancel to go back",
                                     title='Path to the layer ' + str(position+1),
                                     width=50, height=12, init=config_list.get('CONFIG_PATH_TO_LAYER_'+str(position +1), ''))
    if code == d.OK:
        if path_to_metalayer!='':
            config_list['CONFIG_PATH_TO_LAYER_' + str(position + 1)] = path_to_metalayer
            return True
        else:
            d.msgbox("All fields are mandatory!!")
            extra_layer(position)
    else:
        #pass
        return False

def application_integration():
    elements = [
        ("Application name", 1, 1, config_list.get('CONFIG_APPLICATION_NAME',''), 1, 25, 60, 150, 0x0),
        ("Target location", 2, 1, config_list.get('CONFIG_TARGET_LOCATION',''), 2, 25, 60, 150, 0x0)]
    code, fields = d.mixedform(
        'Multiple entries need to be separated by comma.\nUse arrow keys to navigate the menu. Press <tab> to navigate the buttons',
        elements, width=70,height=len(elements)+10, form_height=len(elements)+3,title='Application Integration')
    if code == d.OK:
        if all(fields):
            config_list['CONFIG_APPLICATION_NAME'] = fields[0]
            config_list['CONFIG_TARGET_LOCATION'] = fields[1]
        else:
            d.msgbox("All fields are mandatory!!")
            application_integration()
    else:
        pass
def wifi_config():
    packages_list=['nl802.11']
    code, tags = d.checklist("Arrow keys navigate the menu. use space to select or deselect",
                             choices=[("nl802.11", "", general_packages.get("nl802.11", False))],
                             title="Wifi modules")
    if code == d.OK:
        config_list['CONFIG_WIFI_MODULES'] = tags
        update_selected_packages(tags, packages_list)
    else:
        pass

def kernel_config():

    choices = [("wifi -->", '')]
    code, tag = d.menu(
        "Arrow keys navigate the menu.<Enter> selects submenus ---> "
        "(or empty submenus ----). Highlighted letters are hotkeys. Press <Esc><Esc> or Cancel to go back",
        choices=choices, title="Kernel Configuration", ascii_lines=True,
        height=len(choices) + 12, menu_height=len(choices) + 4, width=70)
    if code == d.OK:
        option = choices.index((tag, '')) + 1
        if option == 1:
            wifi_config()
        #elif option == 2:
        #    genearl_packages()
        kernel_config()
    else:
        pass


def meta_data_layer():
    code, layername = d.inputbox("<Enter> records the entry. Press <Esc><Esc> or Cancel to go back",
                                 title="Meta layer name", width=40, height=10,
                                 init=config_list.get('CONFIG_META_LAYER_NAME', ''))
    if code == d.OK:
        if layername != '':
            config_list['CONFIG_META_LAYER_NAME'] = layername
        else:
            d.msgbox("\"Meta-data layer name\" field should not be empty")
            meta_data_layer()
    else:
        pass

def config_extra_repo():
    code, repocount = d.inputbox("<Enter> records the entry. Press <Esc><Esc> or Cancel to go back. "
                                 "Entry should be of type int",
                                 title="Number of repositories",
                                 width=40, height=10,init=config_list.get('CONFIG_REPO_COUNT',''))
    if code == d.OK:
        config_list['CONFIG_REPO_COUNT'] = repocount
        try:
            if int(repocount):
                for _ in range(int(repocount)):
                    if not extra_repo(_):
                        break
        except Exception as e:
            # print('[INFO][REPOS]', e)
            d.msgbox("\"Number of repositories\" field should be a valid integer")
            config_extra_repo()
    else:
        pass

def config_extra_layer():
    code, layercount = d.inputbox("<Enter> records the entry. Press <Esc><Esc> or Cancel to go back. Entry should be of type int.",
                                  title="Number of Meta layers",width=40, height=10,
                                  init=config_list.get('CONFIG_LAYERS_COUNT',''))
    if code == d.OK:
        try:
            if int(layercount):
                config_list['CONFIG_LAYERS_COUNT'] = layercount
                for _ in range(int(layercount)):
                    if not extra_layer(_):
                        break
        except Exception as e:
            print('[INFO][LAYERS]', e)
            d.msgbox("\"Number of Meta layers\" field should be a valid integer")
            config_extra_layer()
    else:
        pass

def additional_configs():
    elements = [
        ("Hostname", 1, 1, config_list.get("CONFIG_HOST_NAME", ''), 1, 20, 60, 60, 0x0),
        ("Machine name", 2, 1, config_list.get("CONFIG_MACHINE", ''), 2, 20, 60, 60, 0x0),
    ]
    code, fields = d.mixedform(
        'Use arrow keys to navigate the menu. Press <tab> to navigate the buttons. Multiple entries need to be separated by comma.', elements, width=70,
        height=len(elements)+12, form_height=len(elements)+4,title='General Configuration')
    if code == d.OK:
        if fields[0]:
            config_list['CONFIG_HOST_NAME'] = fields[0]
        if all(fields[1:]):
            config_list['CONFIG_MACHINE'] = fields[1]
        else:
            d.msgbox("All fields except Hostname are mandatory!!")
            additional_configs()
    else:
        pass

def service_manager():
    choices = [
        ("systemd", "", general_packages.get("systemd", True)),
        ("sysvinit", "", general_packages.get("sysvinit", False))]
    code, tags = d.radiolist("Arrow keys navigate the menu. use space to select or deselect",
                          title="Service Manager", choices=choices)

    if code == d.OK:
        config_list['CONFIG_SERVICE_MANAGER'] = tags
        update_selected_packages([tags], ['systemd','sysvinit'])
    else:
        pass

def image_file_types():
    packages_list=['cpio','ext4','tar','ubi','ubifs','wic']
    code, tags = d.checklist("Arrow keys navigate the menu. use space to select or deselect",
                             choices=[("cpio", "", general_packages.get("cpio", False)),("ext4", "", general_packages.get("ext4", False)),
                                      ("tar", "", general_packages.get("tar", False)),("ubi", "", general_packages.get("ubi", False)),
                                      ("ubifs", "", general_packages.get("ubifs", False)), ("wic", "", general_packages.get("wic", False))],
                             title="Image File Types")
    if code == d.OK:
        config_list['CONFIG_IMAGE_FILE_TYPE'] = tags
        update_selected_packages(tags, packages_list)
    else:
        pass

def general_config():

    choices = [("Image File types -->", ''), ("Service Manager -->", ''),("General Settings -->",'')]
    code, tag = d.menu(
        "Arrow keys navigate the menu.<Enter> selects submenus ---> "
        "(or empty submenus ----). Highlighted letters are hotkeys. Press <Esc><Esc> or Cancel to go back",
        choices=choices, title="General Machine Settings", ascii_lines=True,
        height=len(choices) + 12, menu_height=len(choices) + 4, width=70)
    if code == d.OK:
        option = choices.index((tag, '')) + 1
        if option == 1:
            image_file_types()
        elif option == 2:
            service_manager()
        elif option == 3:
            additional_configs()
        general_config()
    else:
        pass

def main_menu_select(option):
    if option == 1:
        meta_data_layer()
    elif option == 2:
        config_extra_repo()
    elif option == 3:
        config_extra_layer()
    elif option == 4:
        application_integration()
    elif option == 5:
        required_pakages()
    elif option == 6:
        kernel_config()
    elif option == 7:
        general_config()
    else:
        os.system('clear')
        print("main_menu_select case error")
        exit(1)

def main():
    d.set_background_title("Gadgeon")
    option = None
    menu_level = menu.level1
    while 1:
        if menu_level == menu.level1:
            option = main_menu()
            if option == -1:
                exit_menu()
            else:
                menu_level = menu.level2
        elif menu_level == menu.level2:
            main_menu_select(option)
            menu_level = menu.level1


main()
exit(0)
